<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>ONLINE LECTURE</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>

<body>
    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid p-0">
                    <div class="row align-items-center no-gutters">
                        <div class="col-xl-2 col-lg-2">
                            <div class="logo-img">
                                <a href="index.html">
                                    <img src="img/logo.png" alt="" width="100px">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="active" href="index.html"  style="color:#bf00ff">home</a></li>
                                        <li><a href="Courses.html"  style="color:#bf00ff">Courses</a></li>
                                        <li><a href="#"  style="color:#bf00ff">pages <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="course_details.php">course details</a></li>
                                                <li><a href="lecture_details.php">lecture details</a></li>
                                                <li><a href="colleger_details.php">colleger details</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="about.html"  style="color:#bf00ff">About</a></li>
                                        <li><a href="https://uad.ac.id/en/"  style="color:#bf00ff">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                                <div class="live_chat_btn">
                                    <a class="boxed_btn_orange" href="#">
                                        <i class="fa fa-phone"></i>
                                        <span>+62 378 467 3672</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->

</body>
    <div class="container" style="margin-top:150px">
        <h2>Tambah Data Baru</h2>
        <hr>
        <form action="insertcourse.php" method="post">
            <table>
                <div class="form-group row">
                <label class="col-sm-2 col-form-label">ID COURSE</label>
                <div class="col-sm-10">
                    <input type="text" name="id_course" class="form-control" size="10">
                </div>
            </div>
                <div class="form-group row">
                <label class="col-sm-2 col-form-label">NIY</label>
                <div class="col-sm-10">
                    <select name="niy" class="form-control">
                        <?php
                        include "koneksi.php";
                        $dd = mysqli_query($koneksi,"SELECT*FROM dosen");
                        while($aa=mysqli_fetch_array($dd)){
                            ?>
                            <option value="<?php echo $aa['niy']; ?>"><?php echo $aa['niy']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
                <div class="form-group row">
                <label class="col-sm-2 col-form-label">NIM</label>
                <div class="col-sm-10">
                    <select name="nim" class="form-control">
                        <?php
                        include "koneksi.php";
                        $dd = mysqli_query($koneksi,"SELECT*FROM mahasiswa");
                        while($aa=mysqli_fetch_array($dd)){
                            ?>
                            <option value="<?php echo $aa['nim']; ?>"><?php echo $aa['nim']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
                <div class="form-group row">
                <label class="col-sm-2 col-form-label">CODE COURSE</label>
                <div class="col-sm-10">
                    <select name="kode_matkul" class="form-control">
                        <?php
                        include "koneksi.php";
                        $dd = mysqli_query($koneksi,"SELECT*FROM matakuliah");
                        while($aa=mysqli_fetch_array($dd)){
                            ?>
                            <option value="<?php echo $aa['kode_matkul']; ?>"><?php echo $aa['kode_matkul']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>

                <tr>
                    <td></td>
                    <td><input type="submit" value="Simpan"></td>
                </tr>
            </table>
</form>
</div>

       <!-- footer -->
    <footer class="footer footer_bg_1">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="img/logo.png" alt="" width="100px">
                                </a>
                            </div>
                            <p>
                                Firmament morning sixth subdue darkness creeping gathered divide our let god moving.
                                Moving in fourth air night bring upon it beast let you dominion likeness open place day
                                great.
                            </p>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ti-twitter-alt"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-youtube-play"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                        <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Address
                            </h3>
                            <p>
                                Jl. Kapas No.9, Semaki, Kec. Umbulharjo, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55166 <br>
                                +62 367 467 8934 <br>
                                uad.ac.id
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer -->

   

    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>

    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>

    <script src="js/main.js"></script>
</html>